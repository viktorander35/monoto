//
//  LoginViewController.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/1/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var mSignInButton: UIButton!
    @IBOutlet weak var mCreateAccountButton: UIButton!
    @IBOutlet weak var mUsernameTextfield: UITextField!
    @IBOutlet weak var mPasswordTextfield: UITextField!
    
    var mFrameOrigY:CGFloat!
    let KEYBOARD_OFFSET:CGFloat = 44
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mCreateAccountButton.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mCreateAccountButton.layer.borderWidth = 1
        mCreateAccountButton.layer.borderColor = UIColor.white.cgColor
        mCreateAccountButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        mSignInButton.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mSignInButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        mUsernameTextfield.delegate = self
        mPasswordTextfield.delegate = self
        mFrameOrigY = self.view.frame.origin.y
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        mUsernameTextfield.text = "hank@monotto.com"
        mPasswordTextfield.text = "Snow@123"
        mCreateAccountButton.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //hidden navigation bar
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == Constants.SEGUE_LOGIN_HOME) {
            let containerViewController:ContainerViewController = segue.destination as! ContainerViewController
            containerViewController.mUserData = sender as AnyObject!
        }
    }
    
    // MARK: - Keyboard Action
    
    func keyboardWillShow(_ notification: Notification) {
        if (self.view.frame.origin.y == mFrameOrigY) {
            self.view.frame.origin.y -= KEYBOARD_OFFSET
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if (self.view.frame.origin.y < mFrameOrigY) {
            self.view.frame.origin.y += KEYBOARD_OFFSET
        }
    }
    
    // MARK: - ProgressView Action
    
    func showLoadingProgress () {
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
    }
    
    func hideLoadingProgress () {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    // MARK: - UITextField delegate Action
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == mUsernameTextfield) {
            mPasswordTextfield.becomeFirstResponder()
        } else if (textField == mPasswordTextfield) {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    // MARK: - UIButton Action
    
    @IBAction func signInBtnClicked(_ sender: AnyObject) {
        loginProcess()
    }
    
    @IBAction func createAccountBtnClicked(_ sender: AnyObject) {
        self.performSegue(withIdentifier: Constants.SEGUE_LOGIN_REGISTER, sender: self)
    }
    
    @IBAction func forgotUsernameBtnClicked(_ sender: AnyObject) {
        
    }
    
    @IBAction func forgotPasswordBtnClicked(_ sender: AnyObject) {
        
    }
    
    // MARK: - Login Action
    
    func checkUserInfos() -> Bool {
        let username: String = mUsernameTextfield.text!
        let password: String = mPasswordTextfield.text!
        
        var errorMessage:String!
        if (username.isEmpty) {
            errorMessage = "Username is empty."
        } else if (password.isEmpty) {
            errorMessage = "Password is empty."
        }
        
        if (errorMessage != nil) {
            showErrorAlert(errorMessage)
            return false
        }
        
        return true
    }
    
    func showErrorAlert(_ message: String!) {
        let alert = UIAlertController(title: "Error!", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func loginProcess()
    {
        if (!checkUserInfos()) {
            return
        }
        
        let username: String = mUsernameTextfield.text!
        let password: String = mPasswordTextfield.text!
        
        let url:URL = URL(string: Constants.BASE_URL+"/auth?email="+username+"&password="+password)!
        let session = URLSession.shared
        
        var request = URLRequest(url:url)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        showLoadingProgress ()
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (
            
            data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                print("error")
                return
            }
            
            DispatchQueue.main.async {
                self.hideLoadingProgress ()
            }
            
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                //print(jsonData)
                
                let isSuccess = jsonData["Description"] == nil
                DispatchQueue.main.async {
                    if isSuccess {
                        self.performSegue(withIdentifier: Constants.SEGUE_LOGIN_HOME, sender: jsonData)
                    } else {
                        self.showErrorAlert("Username or password is invalid.")
                    }
                }
            } catch let error as NSError{
                print(error.description)
            }
            
        }) 
        
        task.resume()
        
    }
}
