//
//  SelectBankViewController.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/10/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit

class SelectBankViewController: UIViewController, UITextFieldDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var mMainView:            UIView!
    @IBOutlet weak var mSearchBarBackView:   UIView!
    @IBOutlet weak var mBanksContainerView:  UIView!
    @IBOutlet weak var mBanksListView:       UIView!
    @IBOutlet weak var mSearchBarTextField:  UITextField!
    @IBOutlet weak var mBankingTitleLabel:   UILabel!
    @IBOutlet weak var mContinueButton:      UIButton!
    @IBOutlet weak var mLeftArrowButton:     UIButton!
    @IBOutlet weak var mRightArrowButton:    UIButton!
    
    var mAccessToken:String!
    var mFrameOrigY:CGFloat!
    let KEYBOARD_OFFSET:CGFloat = 44
    
    var mCurrentPageIndex: Int!
    var mSeletedBanksArray:NSArray!
    let mScrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: 200, height: 96))
    var mSubviewFrame: CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mContinueButton.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mContinueButton.layer.borderWidth = 1
        mContinueButton.layer.borderColor = UIColor.white.cgColor
        mContinueButton.titleLabel?.adjustsFontSizeToFitWidth = true
        mContinueButton.isUserInteractionEnabled = false
        mContinueButton.alpha = 0.5
        
        mSearchBarBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        
        mSearchBarTextField.borderStyle = UITextBorderStyle.none
        mSearchBarTextField.attributedPlaceholder = NSAttributedString(string: "Enter Bank Name",
                attributes: [NSForegroundColorAttributeName: UIColor(red: 124.0/255.0, green: 208.0/255.0, blue: 70.0/255.0, alpha: 1.0)])
        
        mSearchBarTextField.delegate = self
        mFrameOrigY = self.view.frame.origin.y
        
        mBanksContainerView.isHidden = true
        mScrollView.delegate = self
        mBanksListView.addSubview(mScrollView)
        
        mCurrentPageIndex = 0
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        mMainView.addGestureRecognizer(leftSwipe)
        mMainView.addGestureRecognizer(rightSwipe)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //hidden navigation bar
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == Constants.SEGUE_SELECT_SIGNINBANK) {
            let signInBankViewController:SignInBankViewController = segue.destination as! SignInBankViewController
            signInBankViewController.mReceiverData = sender as? Dictionary<String, AnyObject>
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Keyboard Action
    
    func keyboardWillShow(_ notification: Notification) {
        if (self.view.frame.origin.y == mFrameOrigY) {
            self.view.frame.origin.y -= KEYBOARD_OFFSET
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if (self.view.frame.origin.y < mFrameOrigY) {
            self.view.frame.origin.y += KEYBOARD_OFFSET
        }
    }
    
    // MARK: - ProgressView Action
    
    func showLoadingProgress () {
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
    }
    
    func hideLoadingProgress () {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    // MARK: - UITextField delegate Action
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == mSearchBarTextField) {
            textField.resignFirstResponder()
            searchBankProcess()
        }
        
        return true
    }
    
    // MARK: - Update views
    
    func updateSearchedBanks () {
        
        for view in self.mScrollView.subviews {
            view.removeFromSuperview()
        }
        
        if (mSeletedBanksArray.count > 0) {
            mContinueButton.isUserInteractionEnabled = true
            mContinueButton.alpha = 1.0
            mBanksContainerView.isHidden = false
            mLeftArrowButton.isHidden = true
            if (mSeletedBanksArray.count > 1) {
                mRightArrowButton.isHidden = false
            } else {
                mRightArrowButton.isHidden = true
            }
            
            for index in 0..<mSeletedBanksArray.count {
                let bankInfo:Dictionary<String, AnyObject>! = mSeletedBanksArray.object(at: index) as? Dictionary<String, AnyObject>
                
                mSubviewFrame.origin.x = self.mScrollView.frame.size.width * CGFloat(index)
                mSubviewFrame.size = self.mScrollView.frame.size
                self.mScrollView.isPagingEnabled = true
                
                let subView = UIView(frame: mSubviewFrame)
                let bankTitleLabel = UILabel(frame: CGRect(x: 40, y: 23, width: 120, height: 51))
                bankTitleLabel.font = UIFont(name: (mBankingTitleLabel.font?.fontName)!, size: 24)
                bankTitleLabel.textColor = UIColor.white
                bankTitleLabel.textAlignment = NSTextAlignment.center
                bankTitleLabel.text = bankInfo["name"] as? String
                bankTitleLabel.numberOfLines = 2
                bankTitleLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                subView.addSubview(bankTitleLabel)
                self.mScrollView .addSubview(subView)
            }
            
            self.mScrollView.contentSize = CGSize(width: self.mScrollView.frame.size.width * CGFloat(mSeletedBanksArray.count),
                                                      height: self.mScrollView.frame.size.height)
            
        } else {
            mContinueButton.isUserInteractionEnabled = false
            mContinueButton.alpha = 0.5
            mBanksContainerView.isHidden = true
        }
    }
    
    func changePage(_ sender: AnyObject) -> () {
        let x = CGFloat(mCurrentPageIndex) * mScrollView.frame.size.width
        mScrollView.setContentOffset(CGPoint(x: x, y: 0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        mCurrentPageIndex = Int(pageNumber)
        
        updateArrowButtons()
    }
    
    func updateArrowButtons() {
        if (mCurrentPageIndex == 0) {
            mLeftArrowButton.isHidden = true
        } else if (mCurrentPageIndex == mSeletedBanksArray.count-1) {
            mRightArrowButton.isHidden = true;
        } else {
            if (mRightArrowButton.isHidden) {
                mRightArrowButton.isHidden = false
            }
            
            if (mLeftArrowButton.isHidden) {
                mLeftArrowButton.isHidden = false
            }
        }
    }
    
    func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .left) {
            rightArrowBtnClicked(NSNull.self)
            
        } else if (sender.direction == .right) {
            leftArrowBtnClicked(NSNull.self)
        }
    }
    
    func searchBankProcess() {
        if (!mSearchBarTextField.text!.isEmpty) {
            let searchText:String = mSearchBarTextField.text! as String
            
            let url:URL = URL(string: Constants.BASE_URL+"/bank/search?q="+searchText)!
            let session = URLSession.shared
            
            var request = URLRequest(url:url)
            request.httpMethod = "GET"
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            request.setValue("Bearer \(mAccessToken)", forHTTPHeaderField: "Authorization")
            
            showLoadingProgress ()
            let task = session.dataTask(with: request as URLRequest, completionHandler: {
                (
                
                data, response, error) in
                
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    print("error")
                    return
                }
                
                DispatchQueue.main.async {
                    self.hideLoadingProgress ()
                }
                
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
                    print(jsonData)
                    self.mSeletedBanksArray = jsonData as! NSArray
                    DispatchQueue.main.async {
                        self.updateSearchedBanks()
                    }
                } catch let error as NSError{
                    print(error.description)
                }
            }) 
            
            task.resume()
        }
    }
    
    // MARK: - UIButton Action
    
    @IBAction func searchBankBtnClicked(_ sender: AnyObject) {
        mSearchBarTextField.resignFirstResponder()
        searchBankProcess()
    }
    
    @IBAction func leftArrowBtnClicked(_ sender: AnyObject) {
        if (mCurrentPageIndex > 0) {
            mCurrentPageIndex = mCurrentPageIndex - 1
            
            updateArrowButtons()
            changePage(NSNull.self)
        }
    }
    
    @IBAction func rightArrowBtnClicked(_ sender: AnyObject) {
        if (mCurrentPageIndex < mSeletedBanksArray.count-1) {
            mCurrentPageIndex = mCurrentPageIndex + 1
            
            updateArrowButtons()
            changePage(NSNull.self)
        }
    }
    
    @IBAction func continueBtnClicked(_ sender: AnyObject) {
        let seletedBankInfo = mSeletedBanksArray.object(at: mCurrentPageIndex)
        var senderData:Dictionary<String, AnyObject>! = [:]
        senderData["access_token"] = mAccessToken as AnyObject?
        senderData["bank_info"] = seletedBankInfo as AnyObject?
        self.performSegue(withIdentifier: Constants.SEGUE_SELECT_SIGNINBANK, sender: senderData)
    }
}
