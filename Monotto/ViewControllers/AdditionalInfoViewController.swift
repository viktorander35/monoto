//
//  AdditionalInfoViewController.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/2/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit

class AdditionalInfoViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var mContinueButton: UIButton!
    @IBOutlet weak var mFirstAnswerBackView: UIView!
    @IBOutlet weak var mSecondAnswerBackView: UIView!
    @IBOutlet weak var mFirstAnswerTextField: UITextField!
    @IBOutlet weak var mSecondAnswerTextField: UITextField!
    
    var mAccessToken:String!
    var mFrameOrigY:CGFloat!
    let KEYBOARD_OFFSET:CGFloat = 150
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mContinueButton.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mContinueButton.layer.borderWidth = 1
        mContinueButton.layer.borderColor = UIColor.white.cgColor
        mContinueButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        mFirstAnswerBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mSecondAnswerBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        
        mFirstAnswerTextField.delegate = self
        mSecondAnswerTextField.delegate = self
        mFrameOrigY = self.view.frame.origin.y
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //hidden navigation bar
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == Constants.SEGUE_ADDINFO_SELBANK) {
            let selectBankViewController:SelectBankViewController = segue.destination as! SelectBankViewController
            selectBankViewController.mAccessToken = sender as! String
        }
    }
    
    // MARK: - Keyboard Action
    
    func keyboardWillShow(_ notification: Notification) {
        if (self.view.frame.origin.y == mFrameOrigY) {
            self.view.frame.origin.y -= KEYBOARD_OFFSET
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if (self.view.frame.origin.y < mFrameOrigY) {
            self.view.frame.origin.y += KEYBOARD_OFFSET
        }
    }
    
    // MARK: - UITextField delegate Action
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == mFirstAnswerTextField) {
            mSecondAnswerTextField.becomeFirstResponder()
        } else if (textField == mSecondAnswerTextField) {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    // MARK: - UIButton Action
    
    @IBAction func continueBtnClicked(_ sender: AnyObject) {
        self.performSegue(withIdentifier: Constants.SEGUE_ADDINFO_SELBANK, sender: mAccessToken)
    }
}
