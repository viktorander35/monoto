//
//  AddBankInfoViewController.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/10/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit

class AddBankInfoViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var mContinueButton: UIButton!
    @IBOutlet weak var mFirstSSNBackView: UIView!
    @IBOutlet weak var mSecondSSNBackView: UIView!
    @IBOutlet weak var mThirdSSNBackView: UIView!
    @IBOutlet weak var mFirstSSNTextField: UITextField!
    @IBOutlet weak var mSecondSSNTextField: UITextField!
    @IBOutlet weak var mThirdSSNTextField: UITextField!
    @IBOutlet weak var mSSNLabel: UILabel!
    
    @IBOutlet weak var mBirthContainerView: UIView!
    @IBOutlet weak var mBirthMonthBackView: UIView!
    @IBOutlet weak var mBirthDayBackView: UIView!
    @IBOutlet weak var mBirthYearBackView: UIView!
    @IBOutlet weak var mBirthMonthLabel: UILabel!
    @IBOutlet weak var mBirthDayLabel: UILabel!
    @IBOutlet weak var mBirthYearLabel: UILabel!
    @IBOutlet weak var mBirthLabel: UILabel!
    
    @IBOutlet weak var mDismissTapView: UIView!
    
    @IBOutlet weak var mMonthTableView: UITableView!
    @IBOutlet weak var mDayTableView: UITableView!
    @IBOutlet weak var mYearTableView: UITableView!
    
    var mStartYearForBirthDay:Int!
    var mIsClickedYear:Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mContinueButton.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mContinueButton.layer.borderWidth = 1
        mContinueButton.layer.borderColor = UIColor.white.cgColor
        mContinueButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        mFirstSSNBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mSecondSSNBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mThirdSSNBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        
        mBirthMonthBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mBirthDayBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mBirthYearBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        
        mFirstSSNTextField.delegate = self
        mSecondSSNTextField.delegate = self
        mThirdSSNTextField.delegate = self
        
        mFirstSSNTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        mSecondSSNTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        mThirdSSNTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        mDismissTapView.addGestureRecognizer(tap)
        
        mIsClickedYear = false
        mStartYearForBirthDay = 1916
        mContinueButton.isEnabled = false
        mContinueButton.alpha = 0.1
        
        updateComboboxList(mMonthTableView)
        updateComboboxList(mDayTableView)
        updateComboboxList(mYearTableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //hidden navigation bar
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK: - UI Update Action
    
    func updateComboboxList(_ tableView: UITableView) {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.separatorColor = UIColor.clear
        tableView.backgroundColor = UIColor(red: 52.0/255.0, green: 124.0/255.0, blue: 132.0/255.0, alpha: 1.0)
        mBirthContainerView.addSubview(tableView)
        tableView.isHidden = true
    }
    
    func updateSSNLabel() {
        mSSNLabel.text = mFirstSSNTextField.text! + "-" + mSecondSSNTextField.text! + "-" + mThirdSSNTextField.text!
    }
    
    func updateBirthLabel() {
        var birthMonthString:String = "MM"
        if (mBirthMonthLabel.text!.range(of: "Month") == nil) {
            birthMonthString = mBirthMonthLabel.text!
        }
        
        var birthDayString:String = "DD"
        if (mBirthDayLabel.text!.range(of: "Day") == nil) {
            birthDayString = mBirthDayLabel.text!
        }
        
        var birthYearString:String = "YYYY"
        if (mBirthYearLabel.text!.range(of: "Year") == nil) {
            birthYearString = mBirthYearLabel.text!
        }
        
        mBirthLabel.text = birthMonthString + "/" + birthDayString + "/" + birthYearString
        updateContinueButton()
    }
    
    func updateContinueButton() {
        let birthString:String = mBirthLabel.text!
        if ((mMonthTableView.isHidden && mDayTableView.isHidden && mYearTableView.isHidden) &&
            (birthString.range(of: "MM") == nil && birthString.range(of: "DD") == nil && birthString.range(of: "YYYY") == nil))
        {
            mContinueButton.isEnabled = true
            mContinueButton.alpha = 1.0
        }
        else {
            mContinueButton.isEnabled = false
            mContinueButton.alpha = 0.1
        }
    }
    
    // MARK: - UITextField Action
    
    func textFieldDidChange(_ textField: UITextField) {
        updateSSNLabel()
    }
    
    // MARK: - UITableViewDelegate Action
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count:Int = 0
        if (tableView == mMonthTableView) {
            count = 12
        } else if (tableView == mDayTableView) {
            count = 31
        } else if (tableView == mYearTableView) {
            count = 100
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        for view in cell.contentView.subviews {
            view.removeFromSuperview()
        }
        
        let cellLabel:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
        cellLabel.textColor = UIColor.white
        cellLabel.textAlignment = NSTextAlignment.center
        
        if (tableView == mYearTableView) {
            cellLabel.text = String((indexPath as NSIndexPath).row + mStartYearForBirthDay)
        } else {
            cellLabel.text = String((indexPath as NSIndexPath).row + 1)
        }
        
        cellLabel.font = UIFont(name: "Arial", size: 10)
        cell.contentView.addSubview(cellLabel)
        
        let cellBorderView:UIView = UIView(frame: CGRect(x: 0, y: 19, width: cell.frame.size.width, height: 1))
        cellBorderView.backgroundColor = UIColor(red: 124.0/255.0, green: 208.0/255.0, blue: 70.0/255.0, alpha: 1.0)
        cell.contentView.addSubview(cellBorderView)
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.isHidden = true
        if (tableView == mMonthTableView) {
            mBirthMonthLabel.text = String((indexPath as NSIndexPath).row + 1)
        } else if (tableView == mDayTableView) {
            mBirthDayLabel.text = String((indexPath as NSIndexPath).row + 1)
        } else if (tableView == mYearTableView) {
            mBirthYearLabel.text = String((indexPath as NSIndexPath).row + mStartYearForBirthDay)
        }
        
        updateBirthLabel()
    }
    
    // MARK: - UIButton Action
    
    @IBAction func monthBtnClicked(_ sender: AnyObject) {
        mMonthTableView.isHidden = !mMonthTableView.isHidden
        updateContinueButton()
    }
    
    @IBAction func dayBtnClicked(_ sender: AnyObject) {
        mDayTableView.isHidden = !mDayTableView.isHidden
        updateContinueButton()
    }
    
    @IBAction func yearBtnClicked(_ sender: AnyObject) {
        if (mIsClickedYear == false) {
            mIsClickedYear = true
            let indexPath = IndexPath(row: 60, section: 0)
            mYearTableView.scrollToRow(at: indexPath, at: .top, animated: false)
        }
        
        mYearTableView.isHidden = !mYearTableView.isHidden
        updateContinueButton()
    }
    
    @IBAction func continueBtnClicked(_ sender: AnyObject) {
        self.performSegue(withIdentifier: Constants.SEGUE_ADDBANK_CHECKEMAIL, sender: self)
    }
}
