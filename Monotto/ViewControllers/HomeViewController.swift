//
//  HomeViewController.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/2/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UIScrollViewDelegate {
    
    enum ActivePage: Int {
        case main = 0
        case accounts
        case banks
        case notifications
    }
    
    @IBOutlet weak var mMainView:          UIView!
    @IBOutlet weak var mAccountsView:      UIView!
    @IBOutlet weak var mBanksView:         UIView!
    @IBOutlet weak var mNotificationsView: UIView!
    
    @IBOutlet weak var mBagesBackView: UIView!
    @IBOutlet weak var mCircleBackView: UIView!
    @IBOutlet weak var mLeftArrowButton: UIButton!
    @IBOutlet weak var mRightArrowButton: UIButton!
    @IBOutlet weak var mBalancePageControl: UIPageControl!
    @IBOutlet weak var mCheckingValueLabel: UILabel!
    @IBOutlet weak var mCheckingTitleLabel: UILabel!
    
    var mNameString: String!
    var mTotalSavings: NSNumber!
    var mTotalInvestments: NSNumber!
    var mTotalLoans: NSNumber!
    var mGoalSavings: NSNumber!
    var mGoalInvestments: NSNumber!
    var mGoalLoans: NSNumber!
    var mCheckingValue: NSNumber!
    
    var mUserData:AnyObject!
    var mCurrentPageIndex: Int!
    let MaxBalancePages = 3
    let mBalanceTitles:NSArray! = ["SAVINGS", "INVESTMENTS", "LOANS"]
    static let pageIndex:String = "pageIndex"
    
    let mScrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: 210, height: 210))
    var mSubviewFrame: CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(mUserData)
        getUserInfos(mUserData)
        
        setActivePage(ActivePage.main)
        
        mCurrentPageIndex = 0
        configurePageControl()
        
        mScrollView.delegate = self
        mCircleBackView.addSubview(mScrollView)
        for index in 0..<MaxBalancePages {
            
            mSubviewFrame.origin.x = self.mScrollView.frame.size.width * CGFloat(index)
            mSubviewFrame.size = self.mScrollView.frame.size
            self.mScrollView.isPagingEnabled = true
            
            let subView = UIView(frame: mSubviewFrame)
            addContainerView(subView)
            updateBalanceValues(subView, pageIndex: index)
            self.mScrollView .addSubview(subView)
        }
        
        self.mScrollView.contentSize = CGSize(width: self.mScrollView.frame.size.width * 3, height: self.mScrollView.frame.size.height)
        mBalancePageControl.addTarget(self, action: #selector(changePage(_:)), for: UIControlEvents.valueChanged)
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        mMainView.addGestureRecognizer(leftSwipe)
        mMainView.addGestureRecognizer(rightSwipe)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        mBagesBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mLeftArrowButton.isHidden = true
        
        updateCheckingValues()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .left) {
            rightArrowBtnClicked(NSNull)
            
        } else if (sender.direction == .right) {
            leftArrowBtnClicked(NSNull)
        }
    }
    
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        mBalancePageControl.numberOfPages = MaxBalancePages
        mBalancePageControl.currentPage = mCurrentPageIndex
        mBalancePageControl.tintColor = UIColor.clear
        mBalancePageControl.pageIndicatorTintColor = UIColor.clear
        mBalancePageControl.currentPageIndicatorTintColor = UIColor.clear
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    func changePage(_ sender: AnyObject) -> () {
        let x = CGFloat(mBalancePageControl.currentPage) * mScrollView.frame.size.width
        mScrollView.setContentOffset(CGPoint(x: x, y: 0), animated: true)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        mCurrentPageIndex = Int(pageNumber)
        mBalancePageControl.currentPage = mCurrentPageIndex
        
        updateArrowButtons()
    }
    
    func updateArrowButtons() {
        
        if (mCurrentPageIndex == 0) {
            mLeftArrowButton.isHidden = true
        } else if (mCurrentPageIndex == MaxBalancePages-1) {
            mRightArrowButton.isHidden = true;
        } else {
            if (mRightArrowButton.isHidden) {
                mRightArrowButton.isHidden = false
            }
            
            if (mLeftArrowButton.isHidden) {
                mLeftArrowButton.isHidden = false
            }
        }
    }
    
    func getUserInfos(_ userData: AnyObject!) {
        
        let jsonUserData:Dictionary! = userData as! Dictionary<String, AnyObject>
        
        mNameString = jsonUserData["name"] as? String
        
        let balance:NSNumber! = jsonUserData["balance"] as? NSNumber
        let totalsDict:Dictionary! = jsonUserData["totals"] as? Dictionary<String, NSNumber>
        mTotalSavings = totalsDict["savings"]! as NSNumber
        mTotalInvestments = totalsDict["loans"]! as NSNumber
        mTotalLoans = totalsDict["investments"]! as NSNumber
        
        let goalsDict:Dictionary! = jsonUserData["goals"] as? Dictionary<String, NSNumber>
        mGoalSavings = goalsDict["savings"]! as NSNumber
        mGoalInvestments = goalsDict["loans"]! as NSNumber
        mGoalLoans = goalsDict["investments"]! as NSNumber
        
        let checkingValue:Float = balance.floatValue
        mCheckingValue = NSNumber(value: checkingValue as Float)
    }
    
    // MARK: - Update UI Action
    
    func setActivePage (_ pageIdx: ActivePage) {
        mMainView.isHidden = pageIdx != ActivePage.main
        mAccountsView.isHidden = pageIdx != ActivePage.accounts
        mBanksView.isHidden = pageIdx != ActivePage.banks
        mNotificationsView.isHidden = pageIdx != ActivePage.notifications
    }
    
    func addContainerView(_ superView: UIView) {
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 105, y: 105), radius: CGFloat(90), startAngle: CGFloat(0),
                                      endAngle: CGFloat(M_PI * 2), clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        
        //change the stroke color
        shapeLayer.strokeColor = UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 0.55).cgColor
    
        //change the line width
        shapeLayer.lineWidth = 25.0
        
        superView.layer.addSublayer(shapeLayer)
    }
    
    func addBalanceView(_ superView: UIView, percent:CGFloat) {
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 105, y: 105), radius: CGFloat(90), startAngle: CGFloat(-0.5 * M_PI),
                                      endAngle: CGFloat(-0.5 * M_PI + 2 * M_PI * Double(percent)), clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        
        //change the stroke color
        shapeLayer.strokeColor = UIColor.white.cgColor
        
        //change the line width
        shapeLayer.lineWidth = 18.0
        
        superView.layer.addSublayer(shapeLayer)
    }
    
    func updateBalanceValues(_ superView: UIView, pageIndex:Int) {
        
        var totalValue:NSNumber!
        var goalValue:NSNumber!
        switch pageIndex {
        case 0:
            totalValue = mTotalSavings
            goalValue = mGoalSavings
            break
        case 1:
            totalValue = mTotalInvestments
            goalValue = mGoalInvestments
            break
        case 2:
            totalValue = mTotalLoans
            goalValue = mGoalLoans
            break
        default:
            break
        }
        
        if (totalValue != nil) {
            let balanceTitleLabel = UILabel(frame: CGRect(x: 40, y: 110, width: 130, height: 29))
            balanceTitleLabel.font = UIFont(name: (mCheckingTitleLabel.font?.fontName)!, size: 18)
            balanceTitleLabel.textColor = UIColor.white
            balanceTitleLabel.textAlignment = NSTextAlignment.center
            superView.addSubview(balanceTitleLabel)
            
            let balanceValueLabel = UILabel(frame: CGRect(x: 45, y: 84, width: 120, height: 29))
            balanceValueLabel.font = UIFont(name: (mCheckingValueLabel.font?.fontName)!, size: 23)
            balanceValueLabel.textColor = UIColor.white
            balanceValueLabel.textAlignment = NSTextAlignment.center
            superView.addSubview(balanceValueLabel)
            
            balanceTitleLabel.text = mBalanceTitles[pageIndex] as? String
            balanceValueLabel.text = "$"+String(format: "%.2f", totalValue.floatValue)
            
            if (goalValue != nil) {
                var percentValue:Float = 0
                if (goalValue.floatValue > 0) {
                    percentValue = totalValue.floatValue/goalValue.floatValue
                }
                
                addBalanceView(superView, percent: CGFloat(percentValue))
            }
        }
    }
    
    func updateCheckingValues() {
        var checkingTitle:String! = ""
        if (mNameString != nil) {
            checkingTitle = mNameString + "'S "
        }
        checkingTitle = checkingTitle + "CHECKING"
        
        mCheckingValueLabel.text = "$"+String(format: "%.2f", mCheckingValue.floatValue)
        mCheckingTitleLabel.text = checkingTitle
    }
    
    // MARK: - UIButton Action
    
    @IBAction func leftArrowBtnClicked(_ sender: AnyObject) {
        if (mCurrentPageIndex > 0) {
            mCurrentPageIndex = mCurrentPageIndex - 1
            mBalancePageControl.currentPage = mCurrentPageIndex
            
            updateArrowButtons()
            changePage(NSNull)
        }
    }
    
    @IBAction func rightArrowBtnClicked(_ sender: AnyObject) {
        if (mCurrentPageIndex < MaxBalancePages-1) {
            mCurrentPageIndex = mCurrentPageIndex + 1
            mBalancePageControl.currentPage = mCurrentPageIndex
            
            updateArrowButtons()
            changePage(NSNull)
        }
    }
    
    @IBAction func showMenuBtnClicked(_ sender: AnyObject) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.ShowSettingsMenuNotification), object: nil)
    }
}

