//
//  ContainerViewController.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/14/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    
    var mUserData:AnyObject!
    
    var leftViewController: UIViewController? {
        willSet{
            if self.leftViewController != nil {
                if self.leftViewController!.view != nil {
                    self.leftViewController!.view!.removeFromSuperview()
                }
                self.leftViewController!.removeFromParentViewController()
            }
        }
        
        didSet{
            
            self.view!.addSubview(self.leftViewController!.view)
            self.addChildViewController(self.leftViewController!)
        }
    }
    
    var rightViewController: UIViewController? {
        willSet {
            if self.rightViewController != nil {
                if self.rightViewController!.view != nil {
                    self.rightViewController!.view!.removeFromSuperview()
                }
                self.rightViewController!.removeFromParentViewController()
            }
        }
        
        didSet{
            
            self.view!.addSubview(self.rightViewController!.view)
            self.addChildViewController(self.rightViewController!)
        }
    }
    
    var menuShown: Bool = false
    
    @IBAction func swipeRight(_ sender: UISwipeGestureRecognizer) {
        //showMenu()
        
    }
    @IBAction func swipeLeft(_ sender: UISwipeGestureRecognizer) {
        //hideMenu()
    }
    
    func showMenu() {
        UIView.animate(withDuration: 0.3, animations: {
            self.rightViewController!.view.frame = CGRect(x: self.view.frame.origin.x + (self.view.frame.width-25), y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { (Bool) -> Void in
                self.menuShown = true
        })
    }
    
    func hideMenu(_ notification: Notification) {
        let pageInfo = notification.object as! NSDictionary
        let pageIndex = pageInfo[HomeViewController.pageIndex] as! Int
        
        let homeViewController: HomeViewController = self.rightViewController as! HomeViewController
        homeViewController.setActivePage(HomeViewController.ActivePage(rawValue: pageIndex)!)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.rightViewController!.view.frame = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { (Bool) -> Void in
                self.menuShown = false
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let homeViewController: HomeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let settingsViewController: SettingsViewController = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        homeViewController.mUserData = mUserData
        
        self.leftViewController = settingsViewController
        self.rightViewController = homeViewController
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMenu), name: NSNotification.Name(rawValue: Constants.ShowSettingsMenuNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideMenu), name: NSNotification.Name(rawValue: Constants.HideSettingsMenuNotification), object: nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.ShowSettingsMenuNotification), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.HideSettingsMenuNotification), object: nil)
    }
}
