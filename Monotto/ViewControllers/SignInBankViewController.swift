//
//  SignInBankViewController.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/10/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit

class SignInBankViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var mContinueButton: UIButton!
    @IBOutlet weak var mUsernameBackView: UIView!
    @IBOutlet weak var mPasswordBackView: UIView!
    @IBOutlet weak var mUsernameTextField: UITextField!
    @IBOutlet weak var mPasswordTextField: UITextField!
    @IBOutlet weak var mBankTitleLabel: UILabel!
    @IBOutlet weak var mBankSubtitleLabel: UILabel!
    @IBOutlet weak var mBankLogoImageView: UIImageView!
    
    var mFrameOrigY:CGFloat!
    let KEYBOARD_OFFSET:CGFloat = 100
    var mAccessToken:String!
    var mReceiverData:Dictionary<String, AnyObject>!
    var mBankId:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mContinueButton.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mContinueButton.layer.borderWidth = 1
        mContinueButton.layer.borderColor = UIColor.white.cgColor
        mContinueButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        mUsernameBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mPasswordBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        
        mUsernameTextField.borderStyle = UITextBorderStyle.none
        mPasswordTextField.borderStyle = UITextBorderStyle.none
        mUsernameTextField.delegate = self
        mPasswordTextField.delegate = self
        mFrameOrigY = self.view.frame.origin.y
        
        mAccessToken = mReceiverData["access_token"] as? String
        let bankInfo:Dictionary<String, AnyObject>! = mReceiverData["bank_info"] as? Dictionary<String, AnyObject>
        mBankId = bankInfo["id"] as! String
        let bankName:String = bankInfo["name"] as! String
        let bankLogo:String! = bankInfo["logo"] as? String
        
        mBankTitleLabel.text = bankName
        mBankSubtitleLabel.isHidden = true
        
        if (bankLogo != nil) {
            let imageData = Data(base64Encoded: bankLogo, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
            let logoImage = UIImage(data: imageData!)
            mBankLogoImageView.image = logoImage
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //hidden navigation bar
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Keyboard Action
    
    func keyboardWillShow(_ notification: Notification) {
        if (self.view.frame.origin.y == mFrameOrigY) {
            self.view.frame.origin.y -= KEYBOARD_OFFSET
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if (self.view.frame.origin.y < mFrameOrigY) {
            self.view.frame.origin.y += KEYBOARD_OFFSET
        }
    }
    
    // MARK: - ProgressView Action
    
    func showLoadingProgress () {
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
    }
    
    func hideLoadingProgress () {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    // MARK: - UITextField delegate Action
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == mUsernameTextField) {
            mPasswordTextField.becomeFirstResponder()
        } else if (textField == mPasswordTextField) {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    // MARK: - Sign in actions
    
    func checkUserInfos() -> Bool {
        let username: String = mUsernameTextField.text!
        let password: String = mPasswordTextField.text!
        
        var errorMessage:String!
        if (username.isEmpty) {
            errorMessage = "Username is empty."
        } else if (password.isEmpty) {
            errorMessage = "Password is empty."
        }
        
        if (errorMessage != nil) {
            showErrorAlert(errorMessage)
            return false
        }
        
        return true
    }
    
    func showErrorAlert(_ message: String!) {
        let alert = UIAlertController(title: "Error!", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func signInBackProcess() {
        if (!checkUserInfos()) {
            return
        }
        
        let username: String = mUsernameTextField.text!
        let password: String = mPasswordTextField.text!
        
        let url:URL = URL(string: Constants.BASE_URL+"/user/bank")!
        let session = URLSession.shared
        
        var request = URLRequest(url:url)
        request.httpMethod = "POST"
        request.setValue("Bearer \(mAccessToken)", forHTTPHeaderField: "Authorization")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        let bodyStr:String = "username="+username+"&password="+password+"&bankID"+mBankId
        request.httpBody = bodyStr.data(using: String.Encoding.utf8)
        
        showLoadingProgress ()
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (
            
            data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                print("error")
                return
            }
            
            DispatchQueue.main.async {
                self.hideLoadingProgress ()
            }
            
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                print(jsonData)
                
                let isSuccess = jsonData["success"] as! Bool
                DispatchQueue.main.async {
                    if isSuccess == true {
                        let token = jsonData["token"] as! String
                        self.performSegue(withIdentifier: Constants.SEGUE_SIGNIN_ADDBANKINFO, sender: token)
                    } else {
                        let errorMessage = jsonData["msg"] as! String
                        self.showErrorAlert(errorMessage)
                    }
                }
            } catch let error as NSError{
                print(error.description)
            }
            
        }) 
        
        task.resume()
    }
    
    func getQuestionsProcess() {
        
        let url:URL = URL(string: Constants.BASE_URL+"/user/bank")!
        let session = URLSession.shared
        
        var request = URLRequest(url:url)
        request.httpMethod = "POST"
        request.setValue("Bearer \(mAccessToken)", forHTTPHeaderField: "Authorization")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        showLoadingProgress ()
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (
            
            data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                print("error")
                return
            }
            
            DispatchQueue.main.async {
                self.hideLoadingProgress ()
            }
            
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
                print(jsonData)
                
            } catch let error as NSError{
                print(error.description)
            }
            
        }) 
        
        task.resume()
    }
    
    // MARK: - UIButton Action
    
    @IBAction func continueBtnClicked(_ sender: AnyObject) {
        getQuestionsProcess()
        //signInBackProcess()
        //self.performSegueWithIdentifier(Constants.SEGUE_SIGNIN_ADDBANKINFO, sender: self)
    }
}

