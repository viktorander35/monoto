//
//  WelcomeViewController.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/1/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var mStartButton: UIButton!
    @IBOutlet weak var mGoToSignInButton: UIButton!
    @IBOutlet weak var mPreviewPageControl: UIPageControl!
    
    @IBOutlet weak var mFirstPreview: UIView!
    @IBOutlet weak var mSecondPreview: UIView!
    @IBOutlet weak var mThirdPreview: UIView!
    @IBOutlet weak var mFourthPreview: UIView!
    
    var mCurrentPageIndex: Int!
    let MaxPreviewPages = 4
    var showPreviewTimer: Timer!
    var mPreviewsArray: NSMutableArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mStartButton.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mStartButton.layer.borderWidth = 1
        mStartButton.layer.borderColor = UIColor.white.cgColor
        mStartButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        mGoToSignInButton.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mGoToSignInButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        mCurrentPageIndex = 0
        
        mPreviewPageControl.numberOfPages = MaxPreviewPages
        mPreviewPageControl.currentPage = mCurrentPageIndex
        mPreviewPageControl.tintColor = UIColor.clear
        mPreviewPageControl.pageIndicatorTintColor = UIColor.clear
        mPreviewPageControl.currentPageIndicatorTintColor = UIColor.clear
        
        mPreviewsArray = [mFirstPreview, mSecondPreview, mThirdPreview, mFourthPreview]
        
        showPreviewTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(updatePreviewPage),
                                                           userInfo: nil, repeats: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //hidden navigation bar
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updatePreviewPage () {
        if (mCurrentPageIndex < MaxPreviewPages - 1) {
            mCurrentPageIndex = mCurrentPageIndex + 1
        } else {
            mCurrentPageIndex = 0
        }
        
        mPreviewPageControl.currentPage = mCurrentPageIndex
        
        for i in 0 ..< mPreviewsArray.count {
            let view: UIView = mPreviewsArray[i] as! UIView
            if i == mCurrentPageIndex {
                view.isHidden = false
            } else {
                view.isHidden = true
            }
        }
    }
    
    // MARK: - UIButton Action
    
    @IBAction func startBtnClicked(_ sender: AnyObject) {
        self.performSegue(withIdentifier: Constants.SEGUE_WEL_REGISTER, sender: self)
    }
    
    @IBAction func goToSignInBtnClicked(_ sender: AnyObject) {
        self.performSegue(withIdentifier: Constants.SEGUE_WEL_LOGIN, sender: self)
    }
    
}
