//
//  CheckEmailViewController.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/12/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit

class CheckEmailViewController: UIViewController {
    
    var goToHomeTimer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        goToHomeTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(goToHomePage),
                                                                  userInfo: nil, repeats: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //hidden navigation bar
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func goToHomePage () {
        goToHomeTimer.invalidate()
        self.performSegue(withIdentifier: Constants.SEGUE_CHECKEMAIL_HOME, sender: self)
    }
}

