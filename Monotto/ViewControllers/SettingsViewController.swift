//
//  SettingsViewController.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/14/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var mMenuTableView: UITableView!
    var items: [String] = ["ACCOUNTS", "BANKS", "NOTIFICATIONS"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mMenuTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.mMenuTableView.separatorColor = UIColor.clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - UITableViewDelegate Action
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = self.mMenuTableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        for view in cell.contentView.subviews {
            view.removeFromSuperview()
        }
        
        let cellLabel:UILabel = UILabel(frame: CGRect(x: 50, y: 8, width: 200, height: 34))
        cellLabel.textColor = UIColor.white
        cellLabel.text = self.items[(indexPath as NSIndexPath).row]
        cellLabel.font = UIFont(name: "Nexa Bold", size: 16)
        cell.contentView.addSubview(cellLabel)
        
        let cellBorderView:UIView = UIView(frame: CGRect(x: 0, y: 43, width: cell.frame.size.width, height: 1))
        cellBorderView.backgroundColor = UIColor(red: 124.0/255.0, green: 208.0/255.0, blue: 70.0/255.0, alpha: 1.0)
        cell.contentView.addSubview(cellBorderView)
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("You selected cell #\(indexPath.row)!")
        let pageInfo = [HomeViewController.pageIndex: (indexPath as NSIndexPath).row+1]
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.HideSettingsMenuNotification), object: pageInfo)
    }
    
    // MARK: - UIButton Action
    
    @IBAction func hideMenuBtnClicked(_ sender: AnyObject) {
        let pageInfo = [HomeViewController.pageIndex: 0]
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.HideSettingsMenuNotification), object: pageInfo)
    }
}
