//
//  RegisterViewController.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/1/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var mContinueButton: UIButton!
    
    @IBOutlet weak var mEmailBackView: UIView!
    @IBOutlet weak var mPasswordBackView: UIView!
    @IBOutlet weak var mConfirmPassBackView: UIView!
    @IBOutlet weak var mPromoCodeBackView: UIView!
    
    @IBOutlet weak var mEmailTextField: UITextField!
    @IBOutlet weak var mPasswordTextField: UITextField!
    @IBOutlet weak var mConfirmPassTextField: UITextField!
    @IBOutlet weak var mPromoCodeTextField: UITextField!
    
    var mFrameOrigY:CGFloat!
    let KEYBOARD_OFFSET:CGFloat = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mContinueButton.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mContinueButton.layer.borderWidth = 1
        mContinueButton.layer.borderColor = UIColor.white.cgColor
        mContinueButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        mEmailBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mPasswordBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mConfirmPassBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        mPromoCodeBackView.layer.cornerRadius = Constants.DEFAULT_CORNER_RADIUS
        
        mFrameOrigY = self.view.frame.origin.y
        mEmailTextField.delegate = self
        mPasswordTextField.delegate = self
        mConfirmPassTextField.delegate = self
        mPromoCodeTextField.delegate = self
        mPromoCodeTextField.autocorrectionType = UITextAutocorrectionType.no
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        mEmailTextField.text = "mytest9@gmail.com"
        mPasswordTextField.text = "Snow@123"
        mConfirmPassTextField.text = "Snow@123"
        mPromoCodeTextField.text = "111"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //hidden navigation bar
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if (segue.identifier == Constants.SEGUE_REGISTER_TERMS) {
//            let termsOfUseViewController:TermsOfUseViewController = segue.destinationViewController as! TermsOfUseViewController
//            termsOfUseViewController.mAccessToken = sender as! String
//        }
//    }
    
    // MARK: - Keyboard Action
    
    func keyboardWillShow(_ notification: Notification) {
        if (self.view.frame.origin.y == mFrameOrigY) {
            self.view.frame.origin.y -= KEYBOARD_OFFSET
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if (self.view.frame.origin.y < mFrameOrigY) {
            self.view.frame.origin.y += KEYBOARD_OFFSET
        }
    }
    
    // MARK: - ProgressView Action
    
    func showLoadingProgress () {
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
    }
    
    func hideLoadingProgress () {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    // MARK: - UITextField delegate Action
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == mEmailTextField) {
            mPasswordTextField.becomeFirstResponder()
        } else if (textField == mPasswordTextField) {
            mConfirmPassTextField.becomeFirstResponder()
        } else if (textField == mConfirmPassTextField) {
            mPromoCodeTextField.becomeFirstResponder()
        } else if (textField == mPromoCodeTextField) {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    // MARK: - UIButton Action
    
    @IBAction func continueBtnClicked(_ sender: AnyObject) {
        //signupProcess()
        self.performSegue(withIdentifier: Constants.SEGUE_REGISTER_TERMS, sender: self)
    }
    
    // MARK: - Login Action
    
    func checkUserInfos() -> Bool {
        let email: String = mEmailTextField.text!
        let password: String = mPasswordTextField.text!
        let confirmPassword: String = mConfirmPassTextField.text!
        
        var errorMessage:String!
        if (email.isEmpty) {
            errorMessage = "Email is empty."
        } else if (password.isEmpty) {
            errorMessage = "Password is empty."
        } else if (confirmPassword.isEmpty) {
            errorMessage = "Confirm Password is empty."
        } else if (password != confirmPassword) {
            errorMessage = "Passwords do not match."
        } else {
            errorMessage = checkIsValidPassword(password)
        }
        
        if (errorMessage != nil) {
            showErrorAlert(errorMessage)
            return false
        }
        
        return true
    }
    
    func checkIsValidPassword(_ password:String) -> String! {
        
        var errorMessage:String!
        if (password.characters.count < 8) {
            errorMessage = "Password must be more than 8 characters."
        } else {
            let capitalLetterRegEx  = ".*[A-Z]+.*"
            let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
            let capitalresult = texttest.evaluate(with: password)
            
            if (capitalresult == false) {
                errorMessage = "Password must include at least one upper case character."
            } else {
                let capitalLetterRegEx  = ".*[a-z]+.*"
                let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
                let lowercaseresult = texttest.evaluate(with: password)
                if (lowercaseresult == false) {
                    errorMessage = "Password must include at least one lower case character."
                } else {
                    let numberRegEx  = ".*[0-9]+.*"
                    let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
                    let numberresult = texttest1.evaluate(with: password)
                    
                    if (numberresult == false) {
                        errorMessage = "Password must include at least one number."
                    } else {
                        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
                        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
                        
                        let specialresult = texttest2.evaluate(with: password)
                        if (specialresult == false) {
                            errorMessage = "Password must include at least one special character."
                        }
                    }
                }
            }
        }

        return errorMessage
    }
    
    func showErrorAlert(_ message: String!) {
        let alert = UIAlertController(title: "Error!", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func signupProcess()
    {
        if (!checkUserInfos()) {
            return
        }
        
        let email: String = mEmailTextField.text!
        let password: String = mPasswordTextField.text!
        let promoCode: String = mPromoCodeTextField.text!
        
        let url:URL = URL(string: Constants.BASE_URL+"/user")!
        let session = URLSession.shared
        
        var request = URLRequest(url:url)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        let bodyStr:String = "email="+email+"&password="+password+"&referalCode"+promoCode
        request.httpBody = bodyStr.data(using: String.Encoding.utf8)
        
        showLoadingProgress ()
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (
            
            data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                print("error")
                return
            }
            
            DispatchQueue.main.async {
                self.hideLoadingProgress ()
            }
            
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                print(jsonData)
                
                let isSuccess = jsonData["success"] as! Bool
                DispatchQueue.main.async {
                    if isSuccess == true {
                        let accessToken = jsonData["access_token"] as! String
                        self.performSegue(withIdentifier: Constants.SEGUE_REGISTER_TERMS, sender: accessToken)
                    } else {
                        let errorMessage = jsonData["msg"] as! String
                        self.showErrorAlert(errorMessage)
                    }
                }
            } catch let error as NSError{
                print(error.description)
            }
            
        }) 
        
        task.resume()
    }
}

