//
//  TermsOfUseViewController.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/2/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit

class TermsOfUseViewController: UIViewController {
    
    @IBOutlet weak var mAcceptTermsButton: UIButton!
    
    var mAccessToken:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mAccessToken = "1RHGuJ3d5EEb3_2_dXy36y8Jp_2n5PwQBTp0yjI5ZETNkjJa63ygEtZBoWWGAr6ccBGeI_hooEv5N8C1zKxs_sL4abj6H6Dd8lfo7IyUsezaygKdOcIb0nX27irZEJNa5BPgTdqmK9ibo6t6WYkJIxqpLgiy389mWfswn7CExIpibZ7ek4IY00ivcWa6J7MF1pARIyIEzxBRpmRNAwHjxP-DDtXs2yTux0NVaR2pUU0JYPmY5rYX9f1WLsCMVvbYbZNh04ghTpTwMldM7EyusAhvPkliMfkRrQ5x0umfU43XqhCTVKG7w0XCcAx58eouRf2jmFzxUk5mN44s78YjnA"
        print("access_token = "+mAccessToken)
        
        mAcceptTermsButton.layer.cornerRadius = 10
        mAcceptTermsButton.layer.borderWidth = 2
        mAcceptTermsButton.layer.borderColor = UIColor.white.cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //hidden navigation bar
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == Constants.SEGUE_TERMS_ADDINFO) {
            let additionalInfoViewController:AdditionalInfoViewController = segue.destination as! AdditionalInfoViewController
            additionalInfoViewController.mAccessToken = sender as! String
        }
    }
    
    // MARK: - UIButton Action
    
    @IBAction func continueBtnClicked(_ sender: AnyObject) {
        self.performSegue(withIdentifier: Constants.SEGUE_TERMS_ADDINFO, sender: mAccessToken)
    }
}
