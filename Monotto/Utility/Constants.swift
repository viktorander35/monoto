//
//  Constants.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/1/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit

class Constants: NSObject {
    //URLs
    static var BASE_URL                     : String = "https://monotto.com/api"
    
    // Constants
    static var DEFAULT_CORNER_RADIUS        : CGFloat = 5
    
    //Segue Identifier
    static var SEGUE_WEL_LOGIN              : String = "welToLoginSegue"
    static var SEGUE_WEL_REGISTER           : String = "welToRegisterSegue"
    static var SEGUE_REGISTER_TERMS         : String = "registerToTermsSegue"
    static var SEGUE_TERMS_ADDINFO          : String = "termsToAddInfoSegue"
    static var SEGUE_LOGIN_REGISTER         : String = "loginToRegisterSegue"
    static var SEGUE_LOGIN_HOME             : String = "loginToHomeSegue"
    static var SEGUE_ADDINFO_SELBANK        : String = "addInfoToSelectBankSegue"
    static var SEGUE_SELECT_SIGNINBANK      : String = "selectToSignInBankSegue"
    static var SEGUE_SIGNIN_ADDBANKINFO     : String = "signInToAddBankInfoSegue"
    static var SEGUE_ADDBANK_CHECKEMAIL     : String = "addBankToCheckEmailSegue"
    static var SEGUE_CHECKEMAIL_HOME        : String = "checkEmailToHomeSegue"
    
    //NSNotification Identifier
    static var ShowSettingsMenuNotification : String = "ShowSettingsMenu"
    static var HideSettingsMenuNotification : String = "HideSettingsMenu"
}
