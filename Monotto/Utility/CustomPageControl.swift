//
//  CustomPageControl.swift
//  Monotto
//
//  Created by Viktor Glishev on 6/8/16.
//  Copyright © 2016 Launchpeer. All rights reserved.
//

import UIKit
import Foundation

class CustomPageControl: UIPageControl {
    let activeImage:UIImage! = UIImage(named: "EmptyDot")
    let inactiveImage:UIImage! = UIImage(named: "WhiteDot")
    override var currentPage: Int {
        //willSet {
        didSet { //so updates will take place after page changed
            self.updateDots()
        }
    }
    
    convenience init() {
        self.init()
        
        self.pageIndicatorTintColor = UIColor.clear
        self.currentPageIndicatorTintColor = UIColor.clear
    }
    
    func updateDots() {
        for i in 0 ..< subviews.count {
            let view: UIView = subviews[i]
            if view.subviews.count == 0 {
                self.addImageViewOnDotView(view, imageSize: activeImage.size)
            }
            let imageView: UIImageView = view.subviews.first as! UIImageView
            imageView.image = self.currentPage == i ? activeImage : inactiveImage
        }
    }
    
    // MARK: - Private
    
    func addImageViewOnDotView(_ view: UIView, imageSize: CGSize) {
        var frame = view.frame
        frame.origin = CGPoint.zero
        frame.size = imageSize
        
        let imageView = UIImageView(frame: frame)
        imageView.contentMode = UIViewContentMode.center
        view.addSubview(imageView)
    }
    
}
